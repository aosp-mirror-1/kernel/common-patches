From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Giuliano Procida <gprocida@google.com>
Date: Thu, 6 Jun 2024 13:11:36 +0100
Subject: ANDROID: arm64: vdso32: support user-supplied flags

This introduces a new environment variable, KCPPFLAGS_COMPAT.

One use-case is to ensure -ffile-prefix-map is passed to the arm32
compiler to normalise compilation directory and make the ELF build ID
reproducible.

Bug: 345452375
Change-Id: I6ae1df58172f4dadeac1dbbee2e3241b704a9256
Signed-off-by: Giuliano Procida <gprocida@google.com>
---
 Documentation/kbuild/kbuild.rst   | 5 +++++
 arch/arm64/kernel/vdso32/Makefile | 4 ++++
 2 files changed, 9 insertions(+)

diff --git a/Documentation/kbuild/kbuild.rst b/Documentation/kbuild/kbuild.rst
--- a/Documentation/kbuild/kbuild.rst
+++ b/Documentation/kbuild/kbuild.rst
@@ -32,6 +32,11 @@ Additional options to pass when preprocessing. The preprocessing options
 will be used in all cases where kbuild does preprocessing including
 building C files and assembler files.
 
+KCPPFLAGS_COMPAT
+----------------
+Additional options to pass to $(CC_COMPAT) when preprocessing C and assembler
+files.
+
 KAFLAGS
 -------
 Additional options to the assembler (for built-in and modules).
diff --git a/arch/arm64/kernel/vdso32/Makefile b/arch/arm64/kernel/vdso32/Makefile
--- a/arch/arm64/kernel/vdso32/Makefile
+++ b/arch/arm64/kernel/vdso32/Makefile
@@ -101,6 +101,10 @@ VDSO_LDFLAGS += -z max-page-size=4096 -z common-page-size=4096
 VDSO_LDFLAGS += -shared --hash-style=sysv --build-id=sha1
 VDSO_LDFLAGS += --orphan-handling=$(CONFIG_LD_ORPHAN_WARN_LEVEL)
 
+# Add user-supplied KCPPFLAGS_COMPAT as the last assignments
+VDSO_CFLAGS += $(KCPPFLAGS_COMPAT)
+VDSO_AFLAGS += $(KCPPFLAGS_COMPAT)
+
 
 # Borrow vdsomunge.c from the arm vDSO
 # We have to use a relative path because scripts/Makefile.host prefixes
